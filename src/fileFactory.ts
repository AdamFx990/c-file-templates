import * as fs from 'fs';

export class FileFactory {
  async createFile(fullPath: string) {
    fs.writeFile(fullPath, '// ' + fullPath, err => {
      if (err) {
        console.error(err);
        return;
      }
    });
  }
}
