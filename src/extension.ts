// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as codium from 'vscode';
import {FileFactory} from './fileFactory';
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: codium.ExtensionContext) {
  // Use the console to output diagnostic information (console.log) and errors
  // (console.error) This line of code will only be executed once when your
  // extension is activated
  console.log(
      'Congratulations, your extension "c-file-templates" is now active!');

  const fileFactory = new FileFactory();

  let disposable = codium.commands.registerCommand(
      'extension.c-file-templates.new-c-header', (uri: codium.Uri) => {
        const fullPath = uri.fsPath + '/test.h';
        codium.window.showWarningMessage('New header file in ' + fullPath);
        fileFactory.createFile(fullPath);
      });
  disposable = codium.commands.registerCommand(
      'extension.c-file-templates.new-c-file', (uri: codium.Uri) => {
        const fullPath = uri.fsPath + '/test.c';
        codium.window.showInformationMessage('New C file in ' + fullPath);
        fileFactory.createFile(fullPath);
      });

  context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}
